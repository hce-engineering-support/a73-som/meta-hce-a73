FILESEXTRAPATHS:prepend := "${THISDIR}/${PN}:"

SRC_URI += " \
    file://${LINUX_VERSION}/${LINUX_VERSION}.${LINUX_SUBVERSION}/1000-arch-arm-add-support-for-HCE-Engineering-A73-BB200-b.patch \
"

KERNEL_CONFIG_FRAGMENTS += "${WORKDIR}/fragments/${LINUX_VERSION}/fragment-01-hce.config "

SRC_URI += "file://${LINUX_VERSION}/fragment-01-hce.config;subdir=fragments"
