# Cleanup rootfs newly created
add_hce_dtb_link() {
        cd ${IMAGE_ROOTFS}/
        ln -s stm32mp157aaa-a73-bb200.dtb stm32mp-stm32mp1.dtb
}

IMAGE_PREPROCESS_COMMAND:append = "add_hce_dtb_link;"
